import uuid
from django.contrib.postgres.fields import JSONField
from django.db import models


class Patient(models.Model):
    GENDERS = [
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other'),
        ('unknown', 'Unknown')
    ]
    identifier = JSONField(default=dict)
    active = models.BooleanField()
    name = JSONField(default=dict)
    telecom = JSONField(default=list, blank=True, null=True)
    gender = models.CharField(max_length=7, choices=GENDERS, blank=True, null=True)
    birthDate = models.DateField(blank=True, null=True)
    deceased = models.DateTimeField(blank=True, null=True)
    address = JSONField(default=list, blank=True, null=True)
    #contact = models.ForeignKey('Contact', on_delete=models.SET_NULL)
    #communication = models.ForeignKey('Communication', on_delete=models.SET_NULL)
    #link = models.ForeignKey('link', on_delete=models.SET_NULL)

    def __str__(self):
        return "paziente"
