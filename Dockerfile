FROM python:3
ENV PYTHONUNBUFFERED 1

LABEL maintainer="pietroghezzi.ch@gmail.com"

# set working directory to /code/
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
# install python dependencies
RUN pip install -r requirements.txt
ADD . /code/

#COPY docker-entrypoint.sh /docker-entrypoint.sh
#RUN chmod +x /docker-entrypoint.sh
