# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 0.1
* Demo init project with django and docker

### How do I get set up? ###

How to run the project

* docker-compose build
* docker-compose up #you can browse 127.0.0.1:8000 to see the application running
* docker ps # to see containers running ip
* docker exec -it [container-ip] bash #enter in container shell
* python manage.py makemigrations
* python manage.py migrate
* python manage.py createsuperuser
* visit localhost:8000/admin to enter in the admin interface
* visit http://localhost:8000/patients for api interface